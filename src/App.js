import React from 'react';
import './App.css';
import MainPage from './components/MainPage';
import ProfilePage from './components/ProfilePage';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path='/' exact component={MainPage} />
          <Route path='/character/:id' exact component={ProfilePage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
