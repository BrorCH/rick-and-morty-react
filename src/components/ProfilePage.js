import React from 'react';
import Character from './Character';
import styles from './ProfilePage.module.css';

function ProfilePage() {


  return (
    <div className={styles.content}>
      <Character/>
    </div>
    );
}

export default ProfilePage;