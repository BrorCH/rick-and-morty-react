import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import styles from './CharacterList.module.css'
import logo from '../misc/rickAndMortyLogo.png'

function CharacterList() {

    let searchWord = "";

    useEffect(() => {
        fetchList(`https://rickandmortyapi.com/api/character/?name=${searchWord}`);
    }, []);

    let [characterList, setCharacters] = useState([]);
    const [nextPage, setNextPage] = useState([]);

    const fetchList = async (url) => {
        const data = await fetch(url);
        const characters = await data.json();
        if (characters.results) {
            setCharacters(characters.results);
        }
        if (characters.info) {
            setNextPage(characters.info);
        }
    }

    function onClickHandle(event) {
        searchWord = event.target.value.toLowerCase();
        fetchList(`https://rickandmortyapi.com/api/character/?name=${searchWord}`);
    }

    function onMoreClick() {
        fetchList(nextPage.next);
    }
    function onBackClick() {
        fetchList(nextPage.prev);
    }

    return (
        <div>
            <img className={styles.logo} src={logo}></img>
            <input className={styles.searchBar} type="text" placeholder="Search..." onChange={onClickHandle} />
            <div className={styles.container}>
                {characterList.map(character => (
                    <div className={styles.characterCard} key={character.id}>
                        <Link to={`/character/${character.id}`}>
                            <div >
                                <img className={styles.image} src={character.image} alt={character.name}></img>
                                <div className={styles.charInfo}>
                                    <h4>{character.name}</h4>
                                    <h4>{character.status}</h4>
                                </div>
                            </div>
                        </Link>
                    </div>
                ))}
            </div>
            <button className={styles.button} onClick={onBackClick}> Back </button>
            <button className={styles.button} onClick={onMoreClick}> Next </button>
        </div>
    );
}
export default CharacterList;
