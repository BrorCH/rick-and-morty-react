import React, { useState, useEffect, useRef } from 'react';
import styles from './Character.module.css';
import logo from '../misc/rickAndMortyLogo.png'
import { Link } from 'react-router-dom';

function Character() {
  let param = useRef();



  useEffect(() => {
    param.current = window.location.pathname.split("/");
    param.current = param.current[param.current.length - 1];
    fetchList();
  }, []);



  const [character, setCharacters] = useState([]);

  const fetchList = async () => {
    const data = await fetch(`https://rickandmortyapi.com/api/character/${
      param.current
      }`
    );
    const characters = await data.json();
    setCharacters(characters);
  }

  return (
    <div>
      <Link  to={`/`}>
        <img className={styles.logo} src={logo}></img>
      </Link>
      <div className={styles.container}>
        <img className={styles.image} src={character.image} alt={character.name}></img>
        <ul className={styles.info}>
          <li>Name: {character.name}</li>
          <li>Status: {character.status}</li>
          <li>Species: {character.species}</li>
          <li>Type: {character.type ? character.type : "Unknown"}</li>
          <li>Gender: {character.gender}</li>
          <li>Origin: {character.origin ? character.origin.name : "Unknown"}</li>
          <li>Current location: {character.location ? character.location.name : "Unknown"}</li>
        </ul>
      </div>
    </div>
  );
}

export default Character;
